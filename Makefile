ifneq ($(KERNELRELEASE),)
obj-m := tw6869.o
else
KDIR ?= /lib/modules/`uname -r`/build
build:
	$(MAKE) -C $(KDIR) M=$$PWD
install: build
	$(MAKE) -C $(KDIR) M=$$PWD modules_install
clean:
	$(MAKE) -C $(KDIR) M=$$PWD clean
endif
